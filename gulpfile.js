// Gulp config
const gulp = require('gulp')
const sass = require('gulp-sass')
const pug = require('gulp-pug')
const browserSync = require('browser-sync').create()
const useref = require('gulp-useref')
const uglify = require('gulp-uglify')
const gulpif = require('gulp-if')
const imagemin = require('gulp-imagemin')
const cache = require('gulp-cache')
const del = require('del')
const runSequence = require('run-sequence')
const autoprefixer = require('gulp-autoprefixer')
const sourcemaps = require('gulp-sourcemaps')
const csso = require('gulp-csso')
const plumber = require('gulp-plumber')
const notify = require('gulp-notify')
const through2 = require('through2')
const gutil = require('gulp-util')
const frontMatter = require('gulp-front-matter')

// Handle errors and alert the user.
function handleErrors() {
  var args = Array.prototype.slice.call(arguments);

  notify.onError({
    title: 'Task Failed! See console.',
    message: "\n\n<%= error.message %>",
    sound: 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
  }).apply(this, args);

  gutil.beep(); // Beep 'sosumi' again

  // Prevent the 'watch' task from stopping
  this.emit('end');
}

// Browser sync live reload
gulp.task('browserSync', () => {
	browserSync.init({
		server: {
			baseDir: 'build'
		},
		port: 8080,
		open: false
	})
})

// Compile sass with gulp-sass
gulp.task('sass', () => {
	return gulp.src('app/scss/main.scss')
    .pipe(plumber({errorHandler: handleErrors}))
	.pipe(sourcemaps.init())
	.pipe(sass().on('eror', sass.logError))
	.pipe(autoprefixer())
	.pipe(csso({
		restructure: false,
		sourceMap: true,
		debug: true
	}))
	.pipe(sourcemaps.write('./'))
	.pipe(gulp.dest('build/assets/css'))
	.pipe(browserSync.reload({
		stream: true
	}))
})

// Compile Pug to Html
gulp.task('pug', () => {
	return gulp.src('app/views/*.pug')
    .pipe(plumber({errorHandler: handleErrors}))
	.pipe(frontMatter({
		property: 'data'
	}))
	.pipe(pug({
		pretty: true
	}))
	.pipe(gulp.dest('build'))
	.pipe(browserSync.reload({
		stream: true
	}))
})

// Minify js
gulp.task('js', () => {
	return gulp.src('app/js/**/*.js')
	.pipe(uglify())
	.pipe(gulp.dest('build/assets/js'))
	.pipe(browserSync.reload({
		stream: true
	}))
})

// Combine file js with gulp-useref
gulp.task('useref', () => {
	return gulp.src('build/index.html')
	.pipe(useref())
	.pipe(gulpif('*.js', uglify()))
	.pipe(gulpif('*.css', csso()))
	.pipe(gulp.dest('build'))
})

// Optimize image
gulp.task('images', () => {
	return gulp.src('app/images/*.+(png|jpg|jpeg|gif|svg)')
	.pipe(cache(imagemin()))
	.pipe(gulp.dest('bulid/images'))
})

// Clear junk file
gulp.task('clean:build', () => {
	return del.sync(['build/**/*', '!build/images', '!build/images/**/*'])
})

// Clear cache in local
gulp.task('clean', () => {
	return del.sync(['build/assets/css/**/*',
		'build/assets/fonts/**/*',
		'build/assets/images/**/*',
		'build/assets/js/**/*'
	])
})

// Clear cache local
gulp.task('cache:clear', function (callback) {
    return cache.clearAll(callback)
})

// Gulp watch syntax
gulp.task('watch', ['browserSync', 'sass', 'pug', 'js'], () => {
	gulp.watch('app/scss/**/*.scss', ['sass'])
	gulp.watch('app/views/**/*.pug', ['pug'])
	gulp.watch('app/js/**/*.js', ['js'])
})

// Combine gulp task
gulp.task('default', (callback) => {
	runSequence('watch', callback)
})
